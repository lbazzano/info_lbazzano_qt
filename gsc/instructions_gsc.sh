# "CF, ""Tile0", "EM3", "Ntrk", "Wtrk", "Nseg" for PFlow jets3
# "Tile0", "EM3", "Ntrk", "Wtrk", "Nseg" for EMTopo jets

# The list of variables used (for calculation of GSC variables:ChargedFraction,Tile0,EM3,Ntrk,Wtrk) is:
jet_EnergyPerSampling
jet_ConstitE
jet_Ntrk1000
jet_Wtrk1000
jet_ChargedFraction
# And for the PunchTrough/Nseg GSC variable, jet_nMuSeg is used. But we don't (can't) apply Nseg.
# Details on how these are used:
Tile0 = (jet_EnergyPerSampling->at(iJ).at(12)+jet_EnergyPerSampling->at(iJ).at(18)) / jet_ConstitE->at(iJ);
EM3   = (jet_EnergyPerSampling->at(iJ).at(3)+jet_EnergyPerSampling->at(iJ).at(7)) / jet_ConstitE->at(iJ);
Ntrk  = jet_Ntrk1000->at(iJ);
Wtrk  = jet_Wtrk1000->at(iJ);
CF    = jet_ChargedFraction->at(iJ);
Nseg  = jet_nMuSeg->at(iJ);




## Install ##
mkdir gsctree
cd gsctree
mkdir source build run
cd source
# Clone needed repositories
git clone ssh://git@gitlab.cern.ch:7999/atlas-jetetmiss-jesjer/MCcalibrations/GSC.git
git clone https://:@gitlab.cern.ch:8443/atlas-jetetmiss-jesjer/tools/JES_ResponseFitter.git
git clone https://github.com/UCATLAS/xAODAnaHelpers.git

## Setup ##
setupATLAS
asetup AnalysisBase,21.2.171,here
cd ../build
cmake ../source
make -j3
source x86_64*/setup.sh
cd ../run

## Run ##
# run from txt with all listed files:
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files testFile.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0 --log-level debug --force direct
# or run from a sinlge rootfile:
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files /eos/user/l/lbazzano/QT/isolatedEff/run/submitDir/data-tree/mc16_13TeV.root --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0 --log-level debug --force direct
# this takes about 10 min and yields in /GSCTest1:
#       hist-files.root


## Fitter ##
# Outputs depend on the applied correction:
python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0/hist-files.root #fit tile0
# Tile0 outputs:
#      Fitted_Tile0_hist-files.root
#      PlotDump_Fitter_response_Tile0.pdf and PlotDump_Fitter_Var_Tile0.pdf
python GSC/scripts/GSCfitter.py -b --correction Inclusive --input /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0/hist-files.root #fit no calib
# Inclusive outputs:
#      Fitted_Inclusive_hist-files.root
#      PlotDump_Fitter_response_Inclusive.pdf

## Smoother ##
python GSC/scripts/GSCsmoother.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0/Fitted_Tile0_hist-files.root


## Plots ##
# (remember to set Batch mode on!!!)
# plotGSCImprovement.py plots the improvement of the central response and resolution across different GSC stages (as opposed to the previous 2 plotting scripts, which each dealt with a single stage at a given iteration). The input files (inputFiles), the name of their stages (corrTypes), and their colors (fileColors) are each hardcoded at the beginning of the code, and should be set by the user. This code also runs over differnt flavor types, including light-quarks, gluons, b-quarks, c-quarks, and inclusively. The output plots are put in the plots/ directory in the same directory that the code is run in.
python source/GSC/scripts/plotGSCImprovement.py --path . --outDir improv --jetType EM
# plotGSCResponse.py plots the usual pt response curves verses VAR for several pt regions in a given eta bin.
python source/GSC/scripts/plotGSCResponse.py --input r21GSC_Tile0/Fitted_Tile0_hist-files.root --outDir Tile0_Tile0 --correction Tile0
# plotSmoothedCurves.py plots the smoothed response curves as a function of pt or of eta, and is ismilar to plotGSCResponse.py. The input is the smoothed histograms that are the output of GSCsmoother.py. The --pts and --etas options work similarly to plotGSCResponse.py, and are "all" by default.
python source/GSC/scripts/plotSmoothedCurves.py --input r21GSC_Tile0/Smoothed_Fitted_Tile0_hist-files.root --outDir smoothed --correction Tile0




## Example Run ##

source Setup.sh

# NONE ################################################################################################################################################################
# in the config:
#  "m_GSCFile"             : "",
#  "m_GSCOrder"            : "",
#python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/r21GSC --log-level debug --force direct
# Tile0
#python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC/hist-files.root
#python GSC/scripts/GSCsmoother.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC/Fitted_Tile0_hist-files.root
# Then create the Tile0-calibration rootfile that will be used for correcting Tile0. (***)
#cp ../r21GSC/Smoothed_Fitted_Tile0_hist-files.root GSC/data/GSCcalib_r21GSC.root

# TILE0 ###############################################################################################################################################################
# in the config:
#  "m_GSCFile"             : "GSC/data/GSCcalib_r21GSC.root",
#  "m_GSCOrder"            : "Tile0",
#python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0 --log-level debug --force direct
# Tile0
#python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0/hist-files.root
#python GSC/scripts/GSCsmoother.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0/Fitted_Tile0_hist-files.root
# EM3
#python GSC/scripts/GSCfitter.py -b --correction EM3 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0/hist-files.root
#python GSC/scripts/GSCsmoother.py -b --correction EM3 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0/Fitted_EM3_hist-files.root
# hadd Smoothed file with the calibration rootfile obtained before (delete the one from the previous step(***)).
#hadd source/GSC/data/GSCcalib_r21GSC.root r21GSC/Smoothed_Fitted_Tile0_hist-files.root r21GSC_Tile0/Smoothed_Fitted_EM3_hist-files.root

# TILE0 + EM3 #########################################################################################################################################################
# in the config:
#  "m_GSCFile"             : "GSC/data/GSCcalib_r21GSC.root",
#  "m_GSCOrder"            : "Tile0,EM3",
#python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0_EM3 --log-level debug --force direct




## RELEASE 22 RUN ##
#  AntiKt4EMPFlow  #

# check distributions if smoothing is not working :
# python GSC/scripts/plotGSCResponse.py -b --correction Tile0 --jetType AntiKt4EMPFlow -d --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample/Fitted_Tile0_hist-files_r22.root
# python GSC/scripts/plotGSCResponse.py -b --correction EM3 --jetType AntiKt4EMPFlow -d --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0/Fitted_EM3_hist-files_r22.root
# python GSC/scripts/GSCfitter.py -b --correction Inclusive --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0/hist-files_r22.root
# to see response before doing anything
#python GSC/scripts/GSCfitter.py -b --correction Inclusive --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample/hist-files_r22.root

# NONE r22 ################################################################################################################################################################
# Run #
# in the config:
#  "m_GSCFile"             : "",
#  "m_GSCOrder"            : "",
python ../source/xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files_r22.txt --inputList --config ../source/GSC/data/config_Hist_GSC.py --submitDir r22_onlineCalibration --force direct

# Tile0
python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction Tile0 --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample/Fitted_Tile0_hist-files_r22.root

# config
rm GSC/data/GSCcalib_r22GSC.root
cp ../run/r22_test_fullSample/Smoothed_Fitted_Tile0_hist-files_r22.root ../source/GSC/data/GSCcalib_r22GSC.root


# TILE0 r22 ###############################################################################################################################################################
# Run #
# in the config:
#  "m_GSCFile"             : "GSC/data/GSCcalib_r22GSC.root",
#  "m_GSCOrder"            : "Tile0",
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files_r22.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0 --force direct
# Tile0
python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction Tile0 --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0/Fitted_Tile0_hist-files_r22.root
# EM3
python GSC/scripts/GSCfitter.py -b --correction EM3 --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction EM3 --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0/Fitted_EM3_hist-files_r22.root
# config
rm GSC/data/GSCcalib_r22GSC.root
hadd GSC/data/GSCcalib_r22GSC.root ../run/r22_test_fullSample/Smoothed_Fitted_Tile0_hist-files_r22.root ../run/r22_test_fullSample_Tile0/Smoothed_Fitted_EM3_hist-files_r22.root


# TILE0 + EM3 r22 #########################################################################################################################################################
# in the config:
#  "m_GSCFile"             : "GSC/data/GSCcalib_r22GSC.root",
#  "m_GSCOrder"            : "Tile0,EM3",
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files_r22.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3 --force direct 
# Tile0
python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction Tile0 --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3/Fitted_Tile0_hist-files_r22.root
# EM3
python GSC/scripts/GSCfitter.py -b --correction EM3 --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction EM3 --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3/Fitted_EM3_hist-files_r22.root
# Ntrk
python GSC/scripts/GSCfitter.py -b --correction Ntrk --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction Ntrk --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3/Fitted_Ntrk_hist-files_r22.root
# config
rm GSC/data/GSCcalib_r22GSC.root
hadd GSC/data/GSCcalib_r22GSC.root ../run/r22_test_fullSample/Smoothed_Fitted_Tile0_hist-files_r22.root ../run/r22_test_fullSample_Tile0/Smoothed_Fitted_EM3_hist-files_r22.root  ../run/r22_test_fullSample_Tile0_EM3/Smoothed_Fitted_Ntrk_hist-files_r22.root


# TILE0 + EM3 + NTRK r22 #########################################################################################################################################################
# in the config:
#  "m_GSCFile"             : "GSC/data/GSCcalib_r22GSC.root",
#  "m_GSCOrder"            : "Tile0,EM3,Ntrk",
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files_r22.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk --force direct
# Tile0
python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction Tile0 --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk/Fitted_Tile0_hist-files_r22.root
# EM3
python GSC/scripts/GSCfitter.py -b --correction EM3 --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction EM3 --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk/Fitted_EM3_hist-files_r22.root
# Ntrk
python GSC/scripts/GSCfitter.py -b --correction Ntrk --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction Ntrk --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk/Fitted_Ntrk_hist-files_r22.root
# Wtrk
python GSC/scripts/GSCfitter.py -b --correction Wtrk --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk/hist-files_r22.root
python GSC/scripts/GSCsmoother.py -b --correction Wtrk --jetType AntiKt4EMPFlow --input /eos/user/l/lbazzano/QT/gsctree/run/r22_test_fullSample_Tile0_EM3_Ntrk/Fitted_Wtrk_hist-files_r22.root
# config
rm GSC/data/GSCcalib_r22GSC.root
hadd GSC/data/GSCcalib_r22GSC.root ../run/r22_test_fullSample/Smoothed_Fitted_Tile0_hist-files_r22.root ../run/r22_test_fullSample_Tile0/Smoothed_Fitted_EM3_hist-files_r22.root  ../run/r22_test_fullSample_Tile0_EM3/Smoothed_Fitted_Ntrk_hist-files_r22.root ../run/r22_test_fullSample_Tile0_EM3_Ntrk/Smoothed_Fitted_Wtrk_hist-files_r22.root






#  and repeat until the corrections on all variables are done...

