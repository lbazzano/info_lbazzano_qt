import glob

inFileFolder = "improvement"
source_path = "../results/gridOutput_mc21_13p6TeV_OnlineDerivedCalibration_dijetHighStats__190922/"+inFileFolder
#results/gridOutput_MC21_R22_OnlineOfflineCalib_260722/improvementOnOff/"+inFileFolder

Resp_image_files = [ f for f in sorted(glob.glob(source_path+'/resp*.png'))]
Resol_image_files = [ f for f in sorted(glob.glob(source_path+'/resol*.png'))]

from fpdf import FPDF
pdf = FPDF(orientation = 'P')

for R,C in zip(Resp_image_files,Resol_image_files):
    print("saved a R and C")
    pdf.add_page()
    pdf.image(R,10,10,180)
    pdf.image(C,10,140,180)
    print(R,C)
print("saving pdf..."+source_path+"/Results_"+inFileFolder+".pdf")
pdf.output(source_path+"/Results_"+inFileFolder+".pdf", "F")
