from ROOT import *
import os,sys
import numpy as n
import array as arr
######################################################################
## Edit
######################################################################

# Path to trees
PATH ="/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput"

TDirectoryFileName = ""

TTreeName = "IsolatedJet_tree"

######################################################################
# DO NOT MODIFY
#####################################################################

# Save info in arrays

# Get list of folders
Folders = []
for folder in os.walk(PATH).next()[1]:
  treePATH     = PATH+"/"+ folder+"/"

  # Loop over files
  for filetree in os.walk(treePATH).next()[2]:
    
    # Open TFile with TTree
    print "Openning "+treePATH+filetree
    treefile = TFile.Open(treePATH+filetree,"UPDATE")
    
    if not treefile:
      print "File("+treePATH+filetree+") not found, exiting"
      sys.exit(0)	
      
    # Open Ntotal 
    cutflowHistName = "MetaData_EventCount";
    cutflowHist     = treefile.Get(cutflowHistName)
    if not cutflowHist:
      print "Metadata Histogram not found, exiting"	  
      sys.exit(0)
    #totalEvents     = cutflowHist.GetBinContent(1) # nEvents initial
    totalEvents     = cutflowHist.GetBinContent(3) # sumOfWeights initial

    print "totalEvents = " + str(totalEvents) 
    
    count=0     
    # Get TTree
    tree = TTree()
    if TDirectoryFileName != "":
      TDir = treefile.GetDirectory(TDirectoryFileName)	    
      tree = TDir.Get(TTreeName)
    else:
      tree = treefile.Get(TTreeName)	
      
    weightreweighted = arr.array('f',[0])  #n.zeros(1, dtype=float)
    newBranch = tree.Branch("weightreweighted",weightreweighted, "weightreweighted/F");
    # Get mcChannelNumber of the file
   
    N = tree.GetEntries()
    for event in xrange(N):
      tree.GetEntry(event)
      # Read weight
      Weight = tree.weight
      # Calculate weightreweighted
      weightreweighted[0] = (Weight/totalEvents)
      #weightreweighted[0] = Weight
      # Fill tree with sample weight
      newBranch.Fill()
      count += 1
      # Show status
      if count % 1000000 == 0:
        print str(count)+" of "+ str(N) +" events processed"
        print "Weight = " + str(Weight)
        print "weightreweighted = " + str(weightreweighted[0])
    # Write TTree
    
    treefile.Write("",TObject.kOverwrite)
    treefile.Close()

print ">>> DONE <<<"
