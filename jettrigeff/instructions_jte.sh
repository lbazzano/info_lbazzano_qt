# Based on https://twiki.cern.ch/twiki/bin/viewauth/Atlas/Run3JetTriggerPerformancePackages
setupATLAS
lsetup git
mkdir jettrigeff
cd jettrigeff
mkdir source build
cd source/
# clone jte
git clone ssh://git@gitlab.cern.ch:7999/atlas-trigger/jet/jetTriggerEfficiencies.git
cd jetTriggerEfficiencies/
git checkout R22-master
mkdir data
cd data
# Example config (need to be placed in source/JetTriggerEfficiencies/data):
#cp /afs/cern.ch/user/j/jbossios/work/public/JetTrigger/MikaelTests/ProduceTurnOns/source/jetTriggerEfficiencies/data/config_MC_AODtoHIST_JVTchains.py .
# simple starting config (Basic svent Selection and Jet Trigger Efficiencies):
cp /afs/cern.ch/user/l/lbazzano/public/config_MC_AODtoHIST.py .
cd ..
# get trigger details:
git clone ssh://git@gitlab.cern.ch:7999/atlas-trigger/jet/jet-trigger-details.git
cd jet-trigger-details/
git checkout dev-py3
# test python:
#python get_trigger_info.py --menuSet 2016 --trigger HLT_j510
python get_trigger_info.py --trigger HLT_5j70c_pf_ftf_presel5j50_L14J15 --menu 2018 --run3 --dontSeekMatch
cp /afs/cern.ch/user/j/jbossios/work/public/JetTrigger/TriggerEfficienciesAthenaMT/v4/source/jetTriggerEfficiencies/jet-trigger-details/rulebook_hacked.py .
cd ../../
# or both I think: git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/atlas-trigger/jet/jetTriggerEfficiencies.git 
# get xAH
git clone git@github.com:UCATLAS/xAODAnaHelpers.git
cd xAODAnaHelpers
git checkout r22/master
cd ../
# CMakeLists must be manually added
cp /afs/cern.ch/user/j/jbossios/work/public/JetTrigger/TriggerEfficienciesAthenaMT/v4/source/CMakeLists.txt .
#asetup AnalysisBase,22.2.24,here #Truth jets merge
#asetup AnalysisBase,22.2.14
asetup AnalysisBase,22.2.88
cd ../build
cmake ../source/
make -j3
source x86*/setup.sh
cd ..


# run local
mkdir run
cd run
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config ../configs/config_MC_AODtoHIST.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --force direct
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config ../configs/config_MC_AODtoHIST.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/mc16_13TeV/AOD.26391606._010311.pool.root.1 --force direct 

#### no calib
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config ../configs/config_MC_AODtoHIST.py --files /eos/user/l/lbazzano/QT/samples/R22_ExSample/mc16_13TeV/AOD.26391606._001171.pool.root.1 --force direct
#### old calib
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config ../configs/config_MC_AODtoHIST.py --files /eos/user/l/lbazzano/QT/samples/mc21/AOD.29327645._000266.pool.root.1 --force direct

### new calib
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config ../configs/config_MC_AODtoHIST.py --files /eos/user/l/lbazzano/QT/samples/mc21/AOD.29313289._000224.pool.root.1 --force direct


#new effs in JETM1 data
# online
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config ../configs/config_MC_AODtoHIST.py --files /eos/user/l/lbazzano/QT/samples/efficiencies/online/DAOD_JETM1.30967290._000478.pool.root.1 --force direct
# offline 
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config ../configs/config_MC_AODtoHIST.py --files /eos/user/l/lbazzano/QT/samples/efficiencies/offline/DAOD_JETM1.30725324._000023.pool.root.1 --force direct


# run grid (be sure to have R22_sampleLists.txt)
cd gridrun
python runGrid.py
## >>> retry(28425574,newOpts={'nFilesPerJob': 200,'nGBPerJob': 4,'nMaxFilesPerJob':500,'forceStaged':False})


# download from grid
cd gridrun
python downloadAndMerge.py --file R22_triggerHist_sampleLists.txt          --type hist     --outPath /eos/user/a/arombola/run/jetTrigEffOutputs
python downloadAndMerge.py --file R22_triggerHist_sampleLists_metadata.txt --type metadata --outPath /eos/user/a/arombola/run/jetTrigEffOutputs
# last run
python downloadAndMerge.py --file R22_triggerHist_sampleLists_r22OnEmu_xShifts_OffEmu_OfflineEmuTDT_2ndTry_230222_metadata.txt     --type metadata     --outPath /eos/user/a/arombola/run/jetTrigEffOutputs_r22OnEmu_xShifts_OffEmu_OfflineEmuTDT_2ndTry_230222/

# analyze
# create /slices directory, edit RetrieveHistosFromTDirectories.py:
python RetrieveHistosFromTDirectories.py
# hadd slices, edit BuildCurves_old.py and run
python BuildCurves_old.py



##########################################
- Triggers under study:
- New triggers (names?):

HLT_6j35_pf_ftf_0eta240_020jvt_presel6c25_L14J15 (not working)
HLT_6j35c_020jvt_pf_ftf_presel6c25_L14J15  (not working)

HLT_2j250_pf_ftf_0eta240_j120_pf_ftf_0eta240_presel2j180XXj80_L1J100 (not working)
HLT_2j250_pf_ftf_0eta240_j120_pf_ftf_0eta240_L1J100

HLT_5j70_pf_ftf_0eta240_presel5j50_L14J15
HLT_5j70c_pf_ftf_presel5j50_L14J15

HLT_4j115_pf_ftf_presel4j85_L13J50
HLT_4j115_pf_ftf_presel4j85_L13J50

HLT_j0_HT1000_L1J100
HLT_j0_HT1000_pf_ftf_L1J100

HLT_j420_pf_ftf_preselj225_L1J100
HLT_j420_pf_ftf_preselj225_L1J100


# see info about these triggers:
python ../source/jetTriggerEfficiencies/jet-trigger-details/get_trigger_info.py --trigger HLT_5j70c_pf_ftf_presel5j50_L14J15 --menu 2018 --run3 --dontSeekMatch




