import numpy as np
import math
import scipy.integrate as integrate
import scipy.special as special
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

tof = 420
ton = 420
sof = 35
son = 35

pts = np.linspace(350, 550,1000)
yof =  []
efof = []
for pt in pts:
  yof.append(((special.erf((pt-tof)*math.pow(sof,-0.5))+1)/2)*math.pow(pt,-4))
  efof.append((special.erf((pt-tof)*math.pow(sof,-0.5))+1)/2)

x99of  = np.interp(0.99, efof, pts)

iqr    = (np.interp(0.84, efof, pts) - np.interp(0.16, efof, pts)) / np.interp(0.5, efof, pts) 
print("iqr:")
print(iqr)

result = integrate.quad(lambda x: (special.erf((x-tof)*math.pow(sof,-0.5))+1)*math.pow(x,-4), 0.1,x99of)
total  = integrate.quad(lambda x: (special.erf((x-tof)*math.pow(sof,-0.5))+1)*math.pow(x,-4), 0.1,1000)
fracof = result[0]/total[0]
print(fracof,x99of)
print("")

plt.plot(pts,yof,'ro',)

frac = []
shifts = np.linspace(-150,150,500)
for shift in shifts:
    yon =  []
    efon = []
    for pt in pts:
      yon.append(((special.erf((pt-ton-shift)*math.pow(son,-0.5))+1)/2)*math.pow(pt,-4))
      efon.append((special.erf((pt-ton-shift)*math.pow(son,-0.5))+1)/2)
    x99on  = np.interp(0.99, efon, pts)
    result = integrate.quad(lambda x: ((special.erf((x-ton-shift)*math.pow(son,-0.5))+1)/2)*math.pow(x,-4), 0.1,x99on)
    total  = integrate.quad(lambda x: ((special.erf((x-ton-shift)*math.pow(son,-0.5))+1)/2)*math.pow(x,-4), 0.1,1000)
    fracon = result[0]/total[0]
    print(ton+shift,fracon,fracon/fracof,x99on)

    frac.append(fracon)
    plt.plot(pts,yon,'k.') #efon yon
plt.ylim(-max(yon),max(yon)*2)
plt.grid()
plt.xlabel("pt")
plt.ylabel("Efficiency")
plt.show()

plt.figure()
plt.plot(shifts,frac,marker='o',label = "online (shifting)")
plt.axhline(fracof,color='r',label="offline")
plt.grid()
plt.xlabel("shift")
plt.ylabel("f@99")
plt.legend()
plt.show()
