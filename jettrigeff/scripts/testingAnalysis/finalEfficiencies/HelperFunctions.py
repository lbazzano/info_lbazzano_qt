#!/bin/bash
import ROOT
import argparse, os, math, sys

#################################################################################################
def GetHistos(inputfile,htype):######################################Agregado
#def GetHistos(inputfile,htype="num"): 
  #Dir=inputfile.GetDirectory('effHists_MyTurnOns')###################Agregado
  #keys = Dir.GetListOfKeys()#########################################Agregado
  keys = inputfile.GetListOfKeys() 
  histograms = []
  for key in keys:
    name = key.GetName()
    histo = ROOT.TH1F()
    #print(key)#######################################################Agregado
    if htype in name:
      Dir.GetObject(name,histo)######################################Agregado
      inputfile.GetObject(name,histo)
      histo.SetDirectory(0)
      histograms.append(histo)
  if len(histograms) == 0:
    print("Couldn't understand which histograms you want, try 'num' or 'denom'; exiting")
    sys.exit(0)
  return histograms

#################################################################################################

def GetListOfStudies(inputfile):
  keys = inputfile.GetListOfKeys()
  studies = [key.GetName().split("_HLT")[0] for key in keys ] # study names will be repeated but there will ordered/paired with curve names
  return studies

#################################################################################################

def GetListOfTriggers(inputfile):
  #Dir=inputfile.GetDirectory('effHists_MyTurnOns')#Agregado
  #keys = Dir.GetListOfKeys()#Agregado
  keys = inputfile.GetListOfKeys()
  triggers = [ "HLT"+ key.GetName().split("HLT_")[1] for key in keys if "num" in key.GetName() ]#Agregado
  #triggers = [ "HLT"+ key.GetName().split("_HLT")[1] for key in keys if "num" in key.GetName() ]
  reftriggers = [ "HLT" + key.GetName().split("HLT_")[2].split("_num")[0] for key in keys if "num" in key.GetName() ]#Agregado
  #reftriggers = [ "HLT" + key.GetName().split("_HLT")[2].split("_num")[0] for key in keys if "num" in key.GetName() ]
  if len(triggers) != len(reftriggers):
    print("Something went wrong, different size in trigger lists, debug me please!")
    sys.exit(0)
  return triggers, reftriggers

#################################################################################################

def buildEfficiencyCurve(numerator,denominator,option='cl=0.683 b(1,1) mode',extraTag='',nrebin=1):#Agregado nrebin
  
  # Rebin histograms:########################### AGREGADO
  rebin=int(nrebin)
  numerator_rebin = numerator.Rebin(rebin)
  denominator_rebin = denominator.Rebin(rebin)
  ##############################################

  curve = ROOT.TGraphAsymmErrors(numerator_rebin,denominator_rebin,option)#AGREGADO _rebin
  #curve = ROOT.TGraphAsymmErrors(numerator,denominator,option)
  #print("CURVE="+str(curve))
  if extraTag!='':
    title = extraTag
  else:
    Tit1= numerator.GetName().split("MyTurnOns_",2)[1].split("_pt[0]_num",2)[0]
    Tit2= numerator.GetName().split("MyTurnOns_",2)[1].split("_pt[0]_num",2)[1]
    title= Tit1 + "-" + Tit2
    if nrebin !=1:
      title=title + "-rebin_"+str(rebin)
    print('Created curve:\t\t\t'+str(title))
    #title = numerator.GetName().split("_HLT",2)[0]+"_HLT" + numerator.GetName().split("_HLT",2)[1]

  curve.SetNameTitle(title,title)
  curve.GetXaxis().SetTitle("leading jet p_{T} [GeV]")
  curve.GetYaxis().SetTitle("Efficiency")
  return curve

#################################################################################################
def SetUpCanvas( setLogX = False):
  # For plotting purposes
  ROOT.gStyle.SetOptTitle(0)
  ROOT.gStyle.SetOptStat(0)
  can = ROOT.TCanvas("c","",1000,900)
  can.SetTopMargin(0.05)
  can.SetRightMargin(0.05)
  can.SetLeftMargin(0.05)
  if setLogX:
    can.SetLogX()

  ROOT.gPad.SetRightMargin(0.08)
  ROOT.gPad.SetTopMargin(0.15)
  ROOT.gPad.SetLeftMargin(0.15)
  return can

#################################################################################################
def SetUpCurve(curve,i):

  markers = [24,25,26,27,28]
  colors  = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed+2, ROOT.kGreen+3]
  curve.SetMarkerColor( colors[i])
  curve.SetMarkerStyle( markers[i] )
  curve.SetLineColor(   colors[i] )
  
  return 

#################################################################################################
def GetGraphsAsymmErr(myInput,method):##########################method: TDT,Emulated
    keys = myInput.GetListOfKeys()
    #print("method: "+method)
    GraphsAsymmErr = []
    for key in keys:
        name = key.GetName()
        graph = ROOT.TGraphAsymmErrors()
        if method in name:
            print ("method in name")
            print(name,graph)
            myInput.GetObject(method,graph)
            GraphsAsymmErr.append(graph)

    if len(GraphsAsymmErr) == 0:
        print("Couldn't understand which method you want, try 'TDT' or 'Emulated'; exiting")
        sys.exit(0)
    return GraphsAsymmErr

#################################################################################################
def GetInterpolatedEfficiency(myInput,Eff,method):##########################method: TDT,Emulated
    Met = myInput.Get(method)
    print(method)
    invGraph_Met = ROOT.TGraph(Met.GetN(),Met.GetY(),Met.GetX())#Hace un TGraph invertido (x,y)->(y,x)=(x',y')
    xMet=invGraph_Met.Eval(Eff,0,"")#Evalua en el eje x'(y)=Eff y escribe el y'(x) correspondiente
    yMet=Met.Eval(xMet,0,"")#Recupera el valor de y
    return xMet, yMet

#################################################################################################
def GetFittedEfficiency(myInput,Eff,method,trigger_value,xminfit_,xmaxfit_):##########################method: TDT,Emulated
    tge = myInput.Get(method)
 
    ## fit the curve:
    # Build a turn on curve copy, override errors and fit in the full range.
    print("Preliminary fit: overriding errors in efficiency curve")
    graph = ROOT.TGraphErrors()
    for p in range(0,graph.GetN()):
      graph.SetPoint(p,tge.GetX()[p],gtge.GetY()[p])
      graph.SetPointError(p,.01,.01)

    ######## Preliminary Fit ########
    # choose range from threshold
    xminfit = float(trigger_value) - 100.
    if xminfit < 0.:
        xminfit = 0.
    xmaxfit = float(trigger_value) + 500.

    # Define fitting function
    func = ROOT.TF1("fit","[0]+[1]*TMath::Erf((x-[2])/[3])",xminfit,xmaxfit)
    func.SetParName(0,"Offset")
    func.SetParName(1,"Norma")
    func.SetParName(2,"Turn-On (50%) [GeV]")
    func.SetParName(3,"Rise [GeV]")

    func.SetParameter(0, 0.5)
    func.SetParameter(1, 0.5)
    func.SetParameter(2, float(trigger_value) - 50) #in GeV
    func.SetParameter(3,20) #in GeV. No idea what this is yet4
    tge.Fit("fit","","", xminfit, xmaxfit)
   
    print("Retrieving some useful parameters from preliminary fit to initialize the final one." )
    minimum  = func.Eval(-1.E6) 
    plateau  = func.Eval(+1.E6) 
    Xat99    = func.GetX(0.99) # plateau
    slope    = func.GetParameter(1)/func.GetParameter(3)*(2./ROOT.TMath.Pi())*1000.   
    #xminfit_ = trigger_value  1.2 #func.GetX(0.7)
    #xmaxfit_ = trigger_value * 2 #2*Xat99 - xminfit_

    turnon   = func.GetParameter(2)
    rise     = func.GetParameter(3)
    '''
    Xat50    = func.GetX(0.5)
    Xat16    = func.GetX(0.16)
    Xat84    = func.GetX(0.84)
    # IQR = [ Q(84)-Q(16) ] / [ 2 * Q(50) ]
    prelimIQR      = (Xat84 - Xat16) / (2*Xat50) 
    print "Calculated preliminary IQR: ", prelimIQR
    '''
    ############################
    ### Fit in a reduced range 
    ############################
    
    print("Performing final fits: new fitting function with 3 free parameters")
    
    # Fitting top part of the graph:
    func2 = ROOT.TF1("fit2","(1+[0])/2+((1-[0])/2)*TMath::Erf((x-[1])/[2])",xminfit,xmaxfit)
    func2.SetParName(0,"a")
    func2.SetParName(1,"Turn-On (50%) (GeV)")
    func2.SetParName(2,"Rise (GeV)")

    func2.SetParameter(0, 0.2)          
    func2.SetParameter(1, turnon) # in GeV           
    func2.SetParameter(2, rise)   # in GeV    

    tge.Fit("fit2"  , "", "", xminfit_ , xmaxfit_)
    tge.GetFunction("fit2").SetLineColor(ROOT.kRed)
    tge.GetFunction("fit2").SetLineWidth(2)

    xMet    = func2.GetX(Eff)
    return xMet, tge

#################################################################################################
def GetIQR(myInput,method):##########################method: TDT,Emulated    
    Eff84, y = GetInterpolatedEfficiency(myInput,0.84,method)
    Eff16, y = GetInterpolatedEfficiency(myInput,0.16,method)
    Eff50, y = GetInterpolatedEfficiency(myInput,0.50,method)
    IQR = (Eff84 - Eff16) / (2.0*Eff50)
    return IQR

#################################################################################################
def GetFracAtEff(myHisto,EffReq,s):

    try:
      # Add numerator tag
      beg = s[::-1].split("_",1)[1][::-1]
      end = s[::-1].split("_",1)[0][::-1]
      triggerNumerator = beg + "_num" + end
      triggerDenominator = beg + "_denom" + end

      # Retrieve histograms:
      print("Retrieving")
      print(triggerNumerator)
      print(triggerDenominator)
      numerator = myHisto.Get(triggerNumerator).Clone()
      denominator = myHisto.Get(triggerDenominator).Clone()
    except:
      # Add numerator tag
      beg = s[::-1].split("_",2)[2][::-1]
      end = s[::-1].split("_",2)[1][::-1]
      end2 = s[::-1].split("_",1)[0][::-1]
      triggerNumerator = beg + "_num" + end+"_"+end2
      triggerDenominator = beg + "_denom" + end+"_"+end2
      
      # Retrieve histograms:
      print("Retrieving")
      print(triggerNumerator)
      print(triggerDenominator)
      numerator = myHisto.Get(triggerNumerator).Clone()
      denominator = myHisto.Get(triggerDenominator).Clone()


    binmax = numerator.FindFixBin(EffReq) 
    #Total = denominator.Integral()
    #Pass = numerator.Integral()
    #Fail = numerator.Integral(0,binmax)
    Total = denominator.GetEntries()
    Pass = numerator.GetEntries()
    Fail = numerator.Integral(0,binmax)
    FracAtEff = Fail/denominator.Integral(0,binmax)

    return FracAtEff , Fail, Pass, Total , numerator , denominator

