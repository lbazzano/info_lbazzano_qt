import ROOT, argparse, os, math, sys, array
import pandas as pd
import numpy as np
from HelperFunctions import GetInterpolatedEfficiency, GetFittedEfficiency, GetIQR, GetFracAtEff

######################################################################################################################################################
######################################################################################################################################################

EtaRegionTag = ""
# Final calibration #

# 5j70
listOfTriggers = [
        'effHists_JetTriggerEfficiencies_TDT_HLT_5j70c_pf_ftf_presel5c50_L14J15_pt[4]_TDT_onlineCalib',
        'effHists_JetTriggerEfficiencies_TDT_HLT_5j70c_pf_ftf_presel5c50_L14J15_pt[4]_TDT_offlineCalib']
study = "regular"
xminfit_ = 70 
xmaxfit_ = 150 
xmin = 50
xmax = 150
infoString = "HLT_5j70c_pf_ftf_presel5c50_L14J15\n\
   signature:                  Jet\n\
   L1:                         L1_4J15\n\
   L1 jet container:           LVL1JetRoIs\n\
   L1 selection:               {'multiplicity': 4, 'ET': 15, 'eta': [0.0, 3.1]}\n\
   HLT jet container preselection:  HLT_AntiKt4EMTopoJets_subjesIS\n\
   HLT preselection:           {'multiplicity': 5, 'ET': 50, 'eta': [0.0, 2.4]}\n\
   HLT jet container:          HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf\n\
   clusters:                   Particle Flow Objects (PFOs, from tracks + EM-scale topoclusters)\n\
   clustering:                 radius 0.4 anti-kt\n\
   calibration steps:          ['pileup subtraction + residual calibration', 'Jet Energy Scale correction', 'calo+track GSC', 'eta and JES in-situ corrections']\n\
   calibration config:         unknown\n\
   HLT selection:              {'multiplicity': 5, 'ET': 70, 'eta': [0.0, 2.8]}\n\
   offline selection recommended:  {'multiplicity': 5, 'eta': [0, 2.0], 'pt': 0.0}\n\
   comment:                    Manual L1\n\
   rulebook entry:             NOT IN RULEBOOK"

# 4j115

listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_4j115_pf_ftf_presel4j85_L13J50_pt[3]_TDT_onlineCalib',
    'effHists_JetTriggerEfficiencies_TDT_HLT_4j115_pf_ftf_presel4j85_L13J50_pt[3]_TDT_offlineCalib']
study = "regular"

xminfit_ = 125
xmaxfit_ = 160
xmin = 95
xmax = 190
infoString = "HLT_4j115_pf_ftf_presel4j85_L13J50\n\
   signature:                  Jet\n\
   L1:                         L1_3J50\n\
   L1 jet container:           LVL1JetRoIs\n\
   L1 selection:               {'multiplicity': 3, 'ET': 50, 'eta': [0.0, 3.1]}\n\
   HLT jet container preselection:  HLT_AntiKt4EMTopoJets_subjesIS\n\
   HLT preselection:           {'multiplicity': 4, 'ET': 85, 'eta': [0.0, 3.2]}\n\
   HLT jet container:          HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf\n\
   clusters:                   Particle Flow Objects (PFOs, from tracks + EM-scale topoclusters)\n\
   clustering:                 radius 0.4 anti-kt\n\
   calibration steps:          ['pileup subtraction + residual calibration', 'Jet Energy Scale correction', 'calo+track GSC', 'eta and JES in-situ corrections']\n\
   calibration config:         unknown\n\
   HLT selection:              {'multiplicity': 4, 'ET': 115, 'eta': [0.0, 3.2]}\n\
   offline selection recommended:  {'multiplicity': 4, 'eta': [0, 2.8000000000000003], 'pt': 0.0}\n\
   comment:                    Manual L1\n\
   rulebook entry:             NOT IN RULEBOOK"

# ht

listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_j0_HT1000_pf_ftf_L1J100_ht_TDT_onlineCalib',
    'effHists_JetTriggerEfficiencies_TDT_HLT_j0_HT1000_pf_ftf_L1J100_ht_TDT_offlineCalib']
study = "regular"
xminfit_ = 850
xmaxfit_ = 1200
xmin = 900
xmax = 1200
infoString = "HLT_j0_HT1000_pf_ftf_L1J100\n\
   signature:                  Jet\n\
   L1:                         L1_J100\n\
   L1 jet container:           LVL1JetRoIs\n\
   L1 selection:               {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}\n\
   HLT jet container:          HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf\n\
   clusters:                   Particle Flow Objects (PFOs, from tracks + EM-scale topoclusters)\n\
   clustering:                 radius 0.4 anti-kt\n\
   calibration steps:          ['pileup subtraction + residual calibration', 'Jet Energy Scale correction', 'calo+track GSC', 'eta and JES in-situ corrections']\n\
   calibration config:         unknown\n\
   HLT selection:              {'HT': 1000, 'multiplicity': 1, 'ET': 30, 'eta': [0.0, 3.2]}\n\
   offline selection recommended:  {'multiplicity': 1, 'eta': [0, 2.8000000000000003], 'pt': 30.0}\n\
   comment:                    Manual L1\n\
   rulebook entry:             NOT IN RULEBOOK"

# j420

listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_TDT',       # _onlineCalib',
    'effHists_JetTriggerEfficiencies_TDT_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated'] # _offlineCalib']
study = "regular"

xminfit_ = 400 
xmaxfit_ = 460
xmin = 350
xmax = 550
infoString = "HLT_j420_pf_ftf_preselj225_L1J100\n\
   signature:                  Jet\n\
   L1:                         L1_J100\n\
   L1 jet container:           LVL1JetRoIs\n\
   L1 selection:               {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}\n\
   HLT jet container preselection:  HLT_AntiKt4EMTopoJets_subjesIS\n\
   HLT preselection:           {'multiplicity': 1, 'ET': 225, 'eta': [0.0, 3.2]}\n\
   HLT jet container:          HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf\n\
   clusters:                   Particle Flow Objects (PFOs, from tracks + EM-scale topoclusters)\n\
   clustering:                 radius 0.4 anti-kt\n\
   calibration steps:          ['pileup subtraction + residual calibration', 'Jet Energy Scale correction', 'calo+track GSC', 'eta and JES in-situ corrections']\n\
   calibration config:         unknown\n\
   HLT selection:              {'multiplicity': 1, 'ET': 420, 'eta': [0.0, 3.2]}\n\
   offline selection recommended:  {'multiplicity': 1, 'eta': [0, 2.8000000000000003], 'pt': 0.0}\n\
   comment:                    Manual L1\n\
   rulebook entry:             NOT IN RULEBOOK"


# 6j35c
'''
listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_6j35c_pf_ftf_presel6c25_L14J15_pt[5]_TDT_onlineCalib',
    'effHists_JetTriggerEfficiencies_TDT_HLT_6j35c_pf_ftf_presel6c25_L14J15_pt[5]_TDT_offlineCalib']
study = "regular"

xminfit_ = 10
xmaxfit_ = 90
xmin = 0
xmax =100
infoString = "HLT_6j35c_pf_ftf_presel6c25_L14J15\n\
   signature:                  Jet\n\
   L1:                         L1_4J15\n\
   L1 jet container:           LVL1JetRoIs\n\
   L1 selection:               {'multiplicity': 4, 'ET': 15, 'eta': [0.0, 3.1]}\n\
   HLT jet container preselection:  HLT_AntiKt4EMTopoJets_subjesIS\n\
   HLT preselection:           {'multiplicity': 6, 'ET': 25, 'eta': [0.0, 2.4]}\n\
   HLT jet container:          HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf\n\
   clusters:                   Particle Flow Objects (PFOs, from tracks + EM-scale topoclusters)\n\
   clustering:                 radius 0.4 anti-kt\n\
   calibration steps:          ['pileup subtraction + residual calibration', 'Jet Energy Scale correction', 'calo+track GSC', 'eta and JES in-situ corrections']\n\
   calibration config:         unknown\n\
   HLT selection:              {'multiplicity': 6, 'ET': 35, 'eta': [0.0, 2.8]}\n\
   offline selection recommended:  {'multiplicity': 6, 'eta': [0, 2.0], 'pt': 0.0}\n\
   comment:                    Manual L1\n\
   rulebook entry:             NOT IN RULEBOOK"

# 6j35c

listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_6j35c_020jvt_pf_ftf_presel6c25_L14J15_pt[5]_TDT_onlineCalib',
    'effHists_JetTriggerEfficiencies_TDT_HLT_6j35c_020jvt_pf_ftf_presel6c25_L14J15_pt[5]_TDT_offlineCalib']
study = "regular"

xminfit_ = 10
xmaxfit_ = 90
xmin = 0
xmax =100
infoString = "HLT_6j35c_020jvt_pf_ftf_presel6c25_L14J15\n\
   signature:                  Jet\n\
   L1:                         L1_4J15\n\
   L1 jet container:           LVL1JetRoIs\n\
   L1 selection:               {'multiplicity': 4, 'ET': 15, 'eta': [0.0, 3.1]}\n\
   HLT jet container preselection:  HLT_AntiKt4EMTopoJets_subjesIS\n\
   HLT preselection:           {'multiplicity': 6, 'ET': 25, 'eta': [0.0, 2.4]}\n\
   HLT jet container:          HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf\n\
   clusters:                   Particle Flow Objects (PFOs, from tracks + EM-scale topoclusters)\n\
   clustering:                 radius 0.4 anti-kt\n\
   calibration steps:          ['pileup subtraction + residual calibration', 'Jet Energy Scale correction', 'calo+track GSC', 'eta and JES in-situ corrections']\n\
   calibration config:         unknown\n\
   HLT selection:              {'multiplicity': 6, 'ET': 35, 'eta': [0.0, 2.8], 'jvt': 0.2}\n\
   offline selection recommended:  {'multiplicity': 6, 'eta': [0, 2.0], 'pt': 0.0}\n\
   comment:                    Manual L1\n\
   rulebook entry:             NOT IN RULEBOOK"
'''
triggerinfo_vec = infoString.split("\n")
#####################
#####################
#####################



# eta study
#shifts = [0,-0.5,-1,-1.5,-2,-2.5,-3,-3.5,-4,-4.5,-5,-6,-7,-8,-9,-10,-11,-12,-13] # eta study
'''
listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib_00eta15_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated',
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_00eta15_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated']
xminfit_ = 435 
xmaxfit_ = 460
xmin = 390
xmax = 480
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 1, 'ET': 420, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 1, 'eta': [0, 1.5], 'pt': 0.0}"

listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib_15eta24_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated',
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_15eta24_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated']
xminfit_ = 435 
xmaxfit_ = 460
xmin = 390
xmax = 480
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 1, 'ET': 420, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 1, 'eta': [1.5,2.4], 'pt': 0.0}"

listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib_24eta60_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated',
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_24eta60_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated']
xminfit_ = 435 
xmaxfit_ = 460
xmin = 390
xmax = 480
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 1, 'ET': 420, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 1, 'eta': [2.4,6.0], 'pt': 0.0}"

# inclusive eta study

'''
#shifts = [0,-0.5,-1,-1.5,-2,-2.5,-3,-3.5,-4,-4.5,-5,-5.5,-6,-7,-8,-9,-10,-12,-15,-20,-30] # inclusive eta
'''

listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib5_HLT_6j35_pf_ftf_0eta240_020jvt_presel6c25_L14J15_pt[5]_Emulated',
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_HLT_6j35_pf_ftf_0eta240_020jvt_presel6c25_L14J15_pt[5]_Emulated',]
xminfit_ = 55
xmaxfit_ = 90
xmin = 8#20
xmax = 100

L1Selection=        "   L1 selection:                 {'multiplicity': 4, 'ET': 15, 'eta': [0.0, 3.1]}"
Comment =           "   (Same as HLT_6j35_pf_ftf_0eta240_020jvt_L14J15)"
Comment2 = ""
Clustering=         "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 6, 'ET': 35, 'eta': [0.0, 2.4], 'jvt': 0.2}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 6, 'eta': [0, 2.0], 'pt': 0.0}"

listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib4_HLT_2j250_pf_ftf_0eta240_j120_pf_ftf_0eta240_presel2j180XXj80_L1J100_pt[1]_Emulated', 
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_HLT_2j250_pf_ftf_0eta240_j120_pf_ftf_0eta240_presel2j180XXj80_L1J100_pt[1]_Emulated']
xminfit_ = 265 
xmaxfit_ = 300 
xmin = 230
xmax = 300
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       "   HLT too Complicated: I chopped off _j120_pf_ftf_0eta240_presel2j180XXj80_L1J100"
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 2, 'ET': 250, 'eta': [0.0, 2.4]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 2, 'eta': [0, 2.0], 'pt': 0.0}"
Comment2 =          "   (Same as HLT_2j250_pf_ftf_0eta240_j120_pf_ftf_0eta240_L1J100)"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib3_HLT_5j70_pf_ftf_0eta240_presel5j50_L14J15_pt[4]_Emulated',
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_HLT_5j70_pf_ftf_0eta240_presel5j50_L14J15_pt[4]_Emulated']
xminfit_ = 85 
xmaxfit_ = 180 
xmin = 50
xmax = 110
L1Selection =       "   L1 selection:                 {'multiplicity': 4, 'ET': 15, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 5, 'ET': 70, 'eta': [0.0, 2.4]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 5, 'eta': [0, 2.0], 'pt': 0.0}"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib3_HLT_4j115_pf_ftf_presel4j85_L13J50_pt[3]_Emulated',
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_HLT_4j115_pf_ftf_presel4j85_L13J50_pt[3]_Emulated']
xminfit_ = 125 
xmaxfit_ = 160
xmin = 95
xmax = 160
L1Selection =       "   L1 selection:                 {'multiplicity': 3, 'ET': 50, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 4, 'ET': 115, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 4, 'eta': [0, 2.8], 'pt': 0.0}"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib15_HLT_j0_HT1000_L1J100_ht_Emulated',
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_HLT_j0_HT1000_L1J100_ht_Emulated']
xminfit_ = 1030 
xmaxfit_ = 1060
xmin = 900
xmax = 1200
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'HT': 1000, 'multiplicity': 1, 'ET': 30, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 1, 'eta': [0, 2.8], 'pt': 30.0}"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_Emulated_onlineCalib7_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated',
    'effHists_JetTriggerEfficiencies_Emulated_offlineCalib_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated']
xminfit_ = 435 
xmaxfit_ = 460
xmin = 390
xmax = 480
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 1, 'ET': 420, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 1, 'eta': [0, 2.8], 'pt': 0.0}"

'''
# TDT EMU study######################################################################################################################
'''
listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_6j35_pf_ftf_0eta240_020jvt_L14J15_pt[5]_TDT',
    'effHists_JetTriggerEfficiencies_TDT_HLT_6j35_pf_ftf_0eta240_020jvt_L14J15_pt[5]_Emulated',]
xminfit_ = 55
xmaxfit_ = 90
xmin = 8#20
xmax = 100

L1Selection=        "   L1 selection:                 {'multiplicity': 4, 'ET': 15, 'eta': [0.0, 3.1]}"
Comment =           "   (Same as HLT_6j35_pf_ftf_0eta240_020jvt_L14J15)"
Comment2 = "preselection had to be removed"
Clustering=         "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 6, 'ET': 35, 'eta': [0.0, 2.4], 'jvt': 0.2}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 6, 'eta': [0, 2.0], 'pt': 0.0}"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_2j250_pf_ftf_0eta240_j120_pf_ftf_0eta240_L1J100_pt[1]_TDT', 
    'effHists_JetTriggerEfficiencies_TDT_HLT_2j250_pf_ftf_0eta240_j120_pf_ftf_0eta240_L1J100_pt[1]_Emulated']
xminfit_ = 265 
xmaxfit_ = 300 
xmin = 230
xmax = 300
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       "   HLT too Complicated: I chopped off _j120_pf_ftf_0eta240_presel2j180XXj80_L1J100"
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 2, 'ET': 250, 'eta': [0.0, 2.4]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 2, 'eta': [0, 2.0], 'pt': 0.0}"
Comment2 = "preselection had to be removed"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_5j70_pf_ftf_0eta240_presel5j50_L14J15_pt[4]_TDT',
    'effHists_JetTriggerEfficiencies_TDT_HLT_5j70_pf_ftf_0eta240_presel5j50_L14J15_pt[4]_Emulated']
xminfit_ = 85 
xmaxfit_ = 180 
xmin = 50
xmax = 110
L1Selection =       "   L1 selection:                 {'multiplicity': 4, 'ET': 15, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 5, 'ET': 70, 'eta': [0.0, 2.4]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 5, 'eta': [0, 2.0], 'pt': 0.0}"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_4j115_pf_ftf_presel4j85_L13J50_pt[3]_TDT',
    'effHists_JetTriggerEfficiencies_TDT_HLT_4j115_pf_ftf_presel4j85_L13J50_pt[3]_Emulated']
xminfit_ = 125 
xmaxfit_ = 160
xmin = 95
xmax = 160
L1Selection =       "   L1 selection:                 {'multiplicity': 3, 'ET': 50, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 4, 'ET': 115, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 4, 'eta': [0, 2.8], 'pt': 0.0}"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_j0_HT1000_L1J100_ht_TDT',
    'effHists_JetTriggerEfficiencies_TDT_HLT_j0_HT1000_L1J100_ht_Emulated']
xminfit_ = 1030 
xmaxfit_ = 1060
xmin = 900
xmax = 1200
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'HT': 1000, 'multiplicity': 1, 'ET': 30, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 1, 'eta': [0, 2.8], 'pt': 30.0}"


listOfTriggers = [
    'effHists_JetTriggerEfficiencies_TDT_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_TDT',
    'effHists_JetTriggerEfficiencies_TDT_HLT_j420_pf_ftf_preselj225_L1J100_pt[0]_Emulated']
xminfit_ = 435 
xmaxfit_ = 460
xmin = 390
xmax = 480
L1Selection =       "   L1 selection:                 {'multiplicity': 1, 'ET': 100, 'eta': [0.0, 3.1]}"
Comment     =       ""
Comment2 = ""
Clustering =        "   clustering:                   radius 0.4 anti-kt"
HLTSelection=       "   HLT selection:                {'multiplicity': 1, 'ET': 420, 'eta': [0.0, 3.2]}"
OfflineSelection=   "   offline selection recommended:{'multiplicity': 1, 'eta': [0, 2.8000000000000003], 'pt': 0.0}"
'''

######################################################################################################################################################
######################################################################################################################################################
ymin = 0
ymax = 1.5
var = listOfTriggers[0].split("_")[-3]
triggerTitle = "HLT_" + listOfTriggers[0].split("HLT_")[1].split("_"+var)[0]
#shift = [shifts[int(listOfTriggers[0].split("Calib")[1].split("_")[0])],0]
shift = [0,0]
ratio = []
legTag = []
def Main(curves,histos,analyze,plot,doFit,eff):

    myCurves = ROOT.TFile(curves,"read")  
    myHistos = ROOT.TFile(histos,"read") 
    #df = pd.DataFrame(columns=('trigger','Var@Eff (I)','Eff (I)', 'Var@Eff (F)','Eff (F)', 'Frac@Eff', 'Pass', 'IQR'))
    df = pd.DataFrame(columns=('Threshold (GeV)','Var@99', 'Frac@99', 'Probe Rate', 'Reference Rate' ,'Trigger id','IQR'))

    if plot:
        c1 = ROOT.TCanvas("c1","c1",10,10,1000,800)
        pad = ROOT.TPad("pad","",0,0,1,1)
        pad.SetFillColor(0);
        pad.SetGrid()
        pad.Range(xmin,ymin,xmax,ymax)
        pad.Draw()
        pad.Update()
        pad.cd()
        #pad.DrawFrame(xmin,ymin,xmax,ymax, ""+triggerTitle+";AntiKt4EMPFlowJets offline jet "+var+" [GeV];Trigger Efficiency")
        pad.DrawFrame(xmin,ymin,xmax,ymax, ""+triggerTitle+";AntiKt4Truth jet "+var+" [GeV];Trigger Efficiency")
        leg = ROOT.TLegend(0.5, 0.4, 0.9, 0.45)
        leg.SetTextSize(0.025)
        leg.SetFillStyle(0)
        leg.SetBorderSize(0)

    markerColor = [1,2,3,4,5,6,7,8,9,10]
    markerStyle = [20,21,22,23,24,25,26,27,28,29]
    numerator = [0] * len(listOfTriggers)
    denominator = [0] * len(listOfTriggers)
    line = [0] * len(listOfTriggers)
    T = 0
    for trigger in listOfTriggers:
        GraphsAsymmErr = myCurves.Get(trigger)
        xInterEff = xFitEff = FracAtEff = IQR = "-"

        # get trigger value
        try:
            if "c" in trigger.split("HLT_")[1].split("_")[0].split("j")[1] :
              trigger_value = int(trigger.split("HLT_")[1].split("_")[0].split("j")[1].split("c")[0])
            else:
              trigger_value = int(trigger.split("HLT_")[1].split("_")[0].split("j")[1])
        except:
            print(trigger+"\n   couldn't read threshold from name")
        if int(trigger_value) == 0:
            trigger_value = int(trigger.split("HLT_")[1].split("_")[1].split("HT")[1])
            print(trigger+"\n  j threshold = 0 Gev, setting HT threshold to "+str(trigger_value)+" GeV")

        if analyze:
            print(myCurves,eff,trigger)
            xInterEff, yInterEff        = GetInterpolatedEfficiency(myCurves,eff,trigger)
            if doFit:
                xFitEff,GraphsAsymmErr  = GetFittedEfficiency(myCurves,eff,trigger,trigger_value,xminfit_,xmaxfit_)
                yFitEff = GraphsAsymmErr.GetFunction("fit2").Eval(xFitEff)
            FracAtEff, Fail, Pass, Total, numerator[T], denominator[T] = GetFracAtEff(myHistos,xFitEff,trigger)# xFitEff or xInterEff
            IQR                         = GetIQR(myCurves,trigger)
        print(trigger_value+shift[T],xFitEff,FracAtEff,Pass/Total,Total/Total,trigger.split("effHists_JetTriggerEfficiencies_TDT_")[1])
        df.loc[len(df)] = [trigger_value+shift[T],xFitEff,FracAtEff,Pass/Total,Total/Total,trigger.split("effHists_JetTriggerEfficiencies_TDT_")[1],IQR]# regular study
        #df.loc[len(df)] = [trigger_value+shift[T],xFitEff,FracAtEff,Pass/Total,Total/Total,trigger.split("effHists_JetTriggerEfficiencies_Emulated_")[1]]# regular study
        #df.loc[len(df)] = [trigger_value+shift[T],xFitEff,FracAtEff,Pass/Total,Total/Total,trigger.split("_")[-1],IQR]# TDT emu study
        if plot:
            pad.cd()
            numerator[T].SetFillColorAlpha(markerColor[T],0.25)
            numerator[T].SetLineColorAlpha(markerColor[T],0.3)
            numerator[T].GetXaxis().SetRangeUser(xmin,xmax)
            numerator[T].GetYaxis().SetRangeUser(ymin,ymax)
            numerator[T].SetStats(0)
            numerator[T].SetTitle("")
            denominator[T].SetFillColorAlpha(42,0.2)#42
            denominator[T].SetLineColorAlpha(42,0.5)
            denominator[T].GetXaxis().SetRangeUser(xmin,xmax)
            denominator[T].GetYaxis().SetRangeUser(ymin,ymax)
            denominator[T].SetStats(0)
            denominator[T].SetTitle("")
            factor = 0.2
            #xmin = trigger_value - 50 #trigger_value/2
            #xmax = trigger_value + 50 #trigger_value
            GraphsAsymmErr.SetMarkerColor(markerColor[T])
            GraphsAsymmErr.GetFunction("fit2").SetLineColor(markerColor[T])
            GraphsAsymmErr.SetLineColor(markerColor[T])
            GraphsAsymmErr.SetMarkerStyle(markerStyle[T])
            c1.SetGrid()
            if trigger == listOfTriggers[0]:
                GraphsAsymmErr.GetXaxis().SetTitle("AntiKt4EMPFlowJets offline jet "+var+" [GeV]")
                GraphsAsymmErr.GetYaxis().SetTitle("Trigger Efficiency")
                GraphsAsymmErr.GetXaxis().SetLimits(xmin,xmax)
                GraphsAsymmErr.GetYaxis().SetLimits(ymin,ymax)
                GraphsAsymmErr.SetTitle(triggerTitle)
                GraphsAsymmErr.Draw("P")
                numZeroCopy = numerator[0].Clone()
                numZeroCopy.GetXaxis().SetRangeUser(trigger_value,trigger_value*1.01);
                scaleFactor = numZeroCopy.GetMaximum()
                overlay = ROOT.TPad("overlay","",0,0,1,1);
                overlay.Draw()
                overlay.SetFillStyle(4000)
                overlay.SetFillColor(0)
                overlay.SetFrameFillStyle(4000)
                overlay.SetLogy()
                overlay.cd()
                denominator[T].Scale(factor/scaleFactor)
                denominator[T].Draw("same AH hist")
                numerator[T].Scale(factor/scaleFactor)
                numerator[T].Draw("same  AH hist")
                pad.cd()
            else:
                GraphsAsymmErr.Draw("P same")
                overlay.cd()
                numerator[T].Scale(factor/scaleFactor)
                numerator[T].Draw("same AH  hist")
                pad.cd()
            if "online" in trigger:
                try:
                  leg.AddEntry(GraphsAsymmErr, trigger.split("JetTriggerEfficiencies_Emulated_")[1].split("_HLT")[0])#+"("+str(trigger_value+shift[T])+" GeV threshold)")
                  legTag.append(trigger.split("JetTriggerEfficiencies_Emulated_")[1].split("_HLT")[0])
                except:
                  # TEMPORAL HASTA TENER OFFLINE leg.AddEntry(GraphsAsymmErr, trigger.split("_TDT_")[2].split("_")[0])#+"("+str(trigger_value+shift[T])+" GeV threshold)")
                  # legTag.append(trigger.split("_TDT_")[2].split("_")[0])
                  # temporalmente:
                  leg.AddEntry(GraphsAsymmErr,trigger)
                  legTag.append(trigger)
            else:
                if study == "regular":
                  try:
                    leg.AddEntry(GraphsAsymmErr, trigger.split("JetTriggerEfficiencies_Emulated_")[1].split("_HLT")[0])# regular study
                    legTag.append(trigger.split("JetTriggerEfficiencies_Emulated_")[1].split("_HLT")[0])
                  except:
                    #leg.AddEntry(GraphsAsymmErr, trigger.split("_TDT_")[2].split("_")[0])# regular study
                    #legTag.append(trigger.split("_TDT_")[2].split("_")[0])
                    # temporalmente:
                    leg.AddEntry(GraphsAsymmErr,trigger)
                    legTag.append(trigger)
                elif study == "tdtemu":
                  #leg.AddEntry(GraphsAsymmErr, trigger.split("_")[-1])# tdt emu study
                  #legTag.append(trigger.split("_")[-1])
                  # temporalmente:
                  leg.AddEntry(GraphsAsymmErr,trigger)
                  legTag.append(trigger)
            line[T] = ROOT.TLine(xFitEff,0,xFitEff,yFitEff)
            line[T].SetLineColor(markerColor[T])
            line[T].Draw("same")
            # text
            pt99 = ROOT.TLatex()
            pt99.SetTextSize(0.02)
            pt99.SetTextColor(markerColor[T])
            pt99.DrawLatex(xFitEff*1.001,yFitEff*0.9-0.1*T,str(round(xFitEff))+" GeV")
            Fpt99 = ROOT.TLatex()
            Fpt99.SetTextSize(0.02)
            Fpt99.SetTextColor(markerColor[T])
            Fpt99.DrawLatex(xFitEff*1.001,yFitEff*0.85-0.1*T,str(round(FracAtEff,2)*100)+" %")

            leg.Draw("same")

            # Atlas Label / dijet events
            ATLAS = ROOT.TLatex()
            ATLAS.SetTextSize(0.05)
            ATLAS.SetTextFont(72)
            ATLAS.DrawLatex(xmin+(xmax-xmin)*0.65,ymax*0.95,"ATLAS")
            ATLAS.SetTextFont(42)
            ATLAS.DrawLatex(xmin+(xmax-xmin)*0.65,ymax*0.95,"             Internal")
            ATLAS.SetTextSize(0.04)
            ATLAS.SetTextFont(42)
            ATLAS.DrawLatex(xmin+(xmax-xmin)*0.75,ymax*0.9,"dijet events")

            # text
            text = ROOT.TLatex()
            text.SetTextSize(0.015)
            text.SetTextFont(82)
            lap = 0
            for triggerinfo in triggerinfo_vec:
              text.DrawLatex(xmin+(xmax-xmin)*0.01,ymax*(0.97-0.02*lap),triggerinfo)
              lap += 1
            #text.DrawLatex(xmin+(xmax-xmin)*0.01,ymax*0.97,L1Selection)
            #text.DrawLatex(xmin+(xmax-xmin)*0.01,ymax*0.94,Clustering)
            #text.DrawLatex(xmin+(xmax-xmin)*0.01,ymax*0.91,HLTSelection)
            #text.DrawLatex(xmin+(xmax-xmin)*0.01,ymax*0.88,OfflineSelection)
            #text.DrawLatex(xmin+(xmax-xmin)*0.01,ymax*0.85,Comment)
            #text.DrawLatex(xmin+(xmax-xmin)*0.01,ymax*0.82,Comment2)
        ratio.append(Pass)
        T += 1
    pass_ratio_onoff = (ratio[0]/ratio[1] - 1)*100
    ratios_tex = ROOT.TLatex()
    ratios_tex.SetTextSize(0.02)
    ratios_tex.DrawLatex(xmin+(xmax-xmin)*0.45,ymax*0.1,legTag[0]+" rate = "+str(ratio[0])+" / "+legTag[1]+" rate = "+str(ratio[1]))
    ratio_tex = ROOT.TLatex()
    ratio_tex.SetTextSize(0.02)
    ratio_tex.DrawLatex(xmin+(xmax-xmin)*0.45,ymax*0.05,"Increase ("+legTag[0]+" rate / "+legTag[1]+" rate - 1)*100: "+str(round(pass_ratio_onoff,2))+" %")

    pd.set_option('display.max_colwidth', None)
    input()
    c1.SaveAs(curves.split("/")[0]+"/"+triggerTitle+EtaRegionTag+"_"+study+"_study.pdf")
    myHistos.Close()
    myCurves.Close()
    print(df)
    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="%prog [options]", formatter_class= argparse.RawTextHelpFormatter)

    # files
    parser.add_argument('-i','--inputHistos',  type=str,dest='inputHistos', nargs='?', default='',    help="The input histograms file")
    parser.add_argument('-c','--inputCurves',  type=str,dest='inputCurves', nargs='?', default='',    help="The input turn-ons file")
    # to do
    parser.add_argument('-a','--analyze',    type=bool ,dest='analyze',   nargs='?', default=True, help="Analyse and print results")
    parser.add_argument('-p','--plot',       type=bool ,dest='plot',     nargs='?', default=True, help="Plot results")
    parser.add_argument('-f','--doFit',      type=bool ,dest='doFit',       nargs='?', default=True, help="do Fits")
    # details
    parser.add_argument('-e','--efficiency', type=float,dest='eff',                    default=0.99,   help="wanted efficency for Frac@Eff")
    parser.add_argument('-b','--batchMode',  type=bool ,dest='batchMode',   nargs='?', default=False, help="set batch mode (graphic display) on or off")

    args = parser.parse_args()

    curves = args.inputCurves
    histos = args.inputHistos
    analyze = args.analyze
    plot    = args.plot
    eff= args.eff
    doFit = args.doFit

    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    ROOT.gErrorIgnoreLevel = ROOT.kError

    Main(curves,histos,analyze,plot,doFit,eff)
