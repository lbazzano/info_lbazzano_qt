other related YIRAs: 
https://its.cern.ch/jira/browse/ATR-25689 
https://its.cern.ch/jira/browse/ATLMCPROD-9890

				# # # LIST OF USED AND UNUSED SAMPLES # # #
#################################################################################################################
DIJET SAMPLEs for efficiencies
#################################################################################################################
# offline calib:
mc21_13p6TeV:mc21_13p6TeV.801165.Py8EG_A14NNPDF23LO_jj_JZ0.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331
mc21_13p6TeV:mc21_13p6TeV.801166.Py8EG_A14NNPDF23LO_jj_JZ1.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331
mc21_13p6TeV:mc21_13p6TeV.801167.Py8EG_A14NNPDF23LO_jj_JZ2.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331
mc21_13p6TeV:mc21_13p6TeV.801168.Py8EG_A14NNPDF23LO_jj_JZ3.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331
mc21_13p6TeV:mc21_13p6TeV.801169.Py8EG_A14NNPDF23LO_jj_JZ4.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331
mc21_13p6TeV:mc21_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331
mc21_13p6TeV:mc21_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331
mc21_13p6TeV:mc21_13p6TeV.801172.Py8EG_A14NNPDF23LO_jj_JZ7.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331
mc21_13p6TeV:mc21_13p6TeV.801173.Py8EG_A14NNPDF23LO_jj_JZ8.deriv.DAOD_JETM1.e8453_s3873_r13829_p5331


# online calib:
mc21_13p6TeV:mc21_13p6TeV.801166.Py8EG_A14NNPDF23LO_jj_JZ1.deriv.DAOD_JETM1.e8453_s3873_r13862_p5337
mc21_13p6TeV:mc21_13p6TeV.801167.Py8EG_A14NNPDF23LO_jj_JZ2.deriv.DAOD_JETM1.e8453_s3873_r13862_p5337
mc21_13p6TeV:mc21_13p6TeV.801168.Py8EG_A14NNPDF23LO_jj_JZ3.deriv.DAOD_JETM1.e8453_s3873_r13862_p5337
mc21_13p6TeV:mc21_13p6TeV.801169.Py8EG_A14NNPDF23LO_jj_JZ4.deriv.DAOD_JETM1.e8453_s3873_r13862_p5337
mc21_13p6TeV:mc21_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.deriv.DAOD_JETM1.e8453_s3873_r13862_p5337
mc21_13p6TeV:mc21_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.deriv.DAOD_JETM1.e8453_s3873_r13862_p5337
mc21_13p6TeV:mc21_13p6TeV.801172.Py8EG_A14NNPDF23LO_jj_JZ7.deriv.DAOD_JETM1.e8453_s3873_r13862_p5337
mc21_13p6TeV:mc21_13p6TeV.801173.Py8EG_A14NNPDF23LO_jj_JZ8.deriv.DAOD_JETM1.e8453_s3873_r13862_p5337


#################################################################################################################
DIJET SAMPLE B (merge)
#################################################################################################################

* MC dijet AODs (new calib) (HLT, offline, truth):                                 (special)
mc21_13p6TeV:mc21_13p6TeV.801166.Py8EG_A14NNPDF23LO_jj_JZ1.merge.AOD.e8453_e8455_s3873_s3874_r13862_r13831
mc21_13p6TeV:mc21_13p6TeV.801167.Py8EG_A14NNPDF23LO_jj_JZ2.merge.AOD.e8453_e8455_s3873_s3874_r13862_r13831
mc21_13p6TeV:mc21_13p6TeV.801168.Py8EG_A14NNPDF23LO_jj_JZ3.merge.AOD.e8453_e8455_s3873_s3874_r13862_r13831
mc21_13p6TeV:mc21_13p6TeV.801169.Py8EG_A14NNPDF23LO_jj_JZ4.merge.AOD.e8453_e8455_s3873_s3874_r13862_r13831
mc21_13p6TeV:mc21_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.merge.AOD.e8453_e8455_s3873_s3874_r13862_r13831
mc21_13p6TeV:mc21_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.merge.AOD.e8453_e8455_s3873_s3874_r13862_r13831
mc21_13p6TeV:mc21_13p6TeV.801172.Py8EG_A14NNPDF23LO_jj_JZ7.merge.AOD.e8453_e8455_s3873_s3874_r13862_r13831
mc21_13p6TeV:mc21_13p6TeV.801173.Py8EG_A14NNPDF23LO_jj_JZ8.merge.AOD.e8453_e8455_s3873_s3874_r13862_r13831
(used for efficiencies and Response and Resolution plots)

* MC dijet merge AODs (old calib)  (HLT, , ):
mc21_13p6TeV:mc21_13p6TeV.801165.Py8EG_A14NNPDF23LO_jj_JZ0.merge.AOD.e8453_e8455_s3873_s3874_r13829_r13831    
mc21_13p6TeV:mc21_13p6TeV.801166.Py8EG_A14NNPDF23LO_jj_JZ1.merge.AOD.e8453_e8455_s3873_s3874_r13829_r13831    
mc21_13p6TeV:mc21_13p6TeV.801167.Py8EG_A14NNPDF23LO_jj_JZ2.merge.AOD.e8453_e8455_s3873_s3874_r13829_r13831    
mc21_13p6TeV:mc21_13p6TeV.801168.Py8EG_A14NNPDF23LO_jj_JZ3.merge.AOD.e8453_e8455_s3873_s3874_r13829_r13831    
mc21_13p6TeV:mc21_13p6TeV.801169.Py8EG_A14NNPDF23LO_jj_JZ4.merge.AOD.e8453_e8455_s3873_s3874_r13829_r13831    
mc21_13p6TeV:mc21_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.merge.AOD.e8453_e8455_s3873_s3874_r13829_r13831    
mc21_13p6TeV:mc21_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.merge.AOD.e8453_e8455_s3873_s3874_r13829_r13831   
(unused)

* MC dijet DAOD_JETM1 (old calib):
(unused)

* MC dijet DAOD_PHYS (old calib)  (HLT, , ):
mc21_13p6TeV:mc21_13p6TeV.801165.Py8EG_A14NNPDF23LO_jj_JZ0.deriv.DAOD_PHYS.e8453_s3873_r13829_p5278
mc21_13p6TeV:mc21_13p6TeV.801166.Py8EG_A14NNPDF23LO_jj_JZ1.deriv.DAOD_PHYS.e8453_s3873_r13829_p5278
mc21_13p6TeV:mc21_13p6TeV.801167.Py8EG_A14NNPDF23LO_jj_JZ2.deriv.DAOD_PHYS.e8453_s3873_r13829_p5278
mc21_13p6TeV:mc21_13p6TeV.801168.Py8EG_A14NNPDF23LO_jj_JZ3.deriv.DAOD_PHYS.e8453_s3873_r13829_p5278
mc21_13p6TeV:mc21_13p6TeV.801169.Py8EG_A14NNPDF23LO_jj_JZ4.deriv.DAOD_PHYS.e8453_s3873_r13829_p5278
mc21_13p6TeV:mc21_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.deriv.DAOD_PHYS.e8453_s3873_r13829_p5278
mc21_13p6TeV:mc21_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.deriv.DAOD_PHYS.e8453_s3873_r13829_p5278
(I would need offline to do efficiency, waiting answer from  P-A)
in the meanwhile calibrate by hand and compare
question: should these two samples share the constituent scale jets?
(tom: dont forget about HLT vertex, try that if it is not the same)


--> MC request for tau CP and tau trigger, and also jet trigger, dijet and gamma* 		(https://its.cern.ch/jira/browse/ATLMCPROD-10099)
 \------> MC Request for Pythia8 LO Dijet for HLT Jet Calibration Validation		(https://its.cern.ch/jira/browse/ATR-25920)
 \------> MC request for tau CP and tau trigger						(https://its.cern.ch/jira/browse/ATLMCPROD-10089)

#################################################################################################################

MC ttbar (small) AODs (old calib, Trigger.Jet.useTriggerCalib=False):
mc21.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8357_e7400_s3775_r13614_r13709 (Old calibration)
(efficiencies, got same trigger counts as claire)

MC ttbar (small) AODs (new calib, Trigger.Jet.useTriggerCalib=True):
mc21.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8357_e7400_s3775_r13614_r13705 (New calibration)
(efficiencies, got same trigger counts as claire)

--> MC small production request for validation of jet trigger calibration 			(https://its.cern.ch/jira/browse/ATR-25700)		Jun 09

#################################################################################################################
DIJET SAMPLE A (recon)
#################################################################################################################

(response and resolution plots)
(used for derivation)
MC dijet AODs:
mc16_13TeV:mc16_13TeV.800036.Py8EG_A14N23LO_jetjet_JZ1WwithSW.recon.AOD.e7914_s3126_d1677_r12711
mc16_13TeV:mc16_13TeV.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.recon.AOD.e7142_s3126_d1677_r12711
mc16_13TeV:mc16_13TeV.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.recon.AOD.e7142_s3126_d1677_r12711
mc16_13TeV:mc16_13TeV.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.recon.AOD.e7142_s3126_d1677_r12711
mc16_13TeV:mc16_13TeV.364705.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5WithSW.recon.AOD.e7142_s3126_d1677_r12711
mc16_13TeV:mc16_13TeV.364706.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6WithSW.recon.AOD.e7142_s3126_d1677_r12711
