git clone ssh://git@gitlab.cern.ch:7999/privaden/residualcalibration.git
cd residualcalibration

## 1D ##
# edit conifg and input file and run
python DeriveResidualCorrection.py
# this will create inside TH3histograms/
TH3_Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.root
# and inside the residual/
NPVTermOption1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.config
NPVTermOption1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.root
MuTermOption1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.config
MuTermOption1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.root
# these names can be scpecified in the config.

# last two DeriveResidualCorrection.py commands may fail, do this (or compile before):
cd ./MkClosurePlots
g++ -o MkClosurePlots MkClosurePlots.cxx `root-config --cflags --glibs`
cd ..
./MkClosurePlots/MkClosurePlots TH3histograms/TH3_Option1_13122020_139fb_ptfrom10_v2AntiKt4EMPFlowJets.root macros/ExampleConfig_nathan.config NPVTerm average
./MkClosurePlots/MkClosurePlots TH3histograms/TH3_Option1_13122020_139fb_ptfrom10_v2AntiKt4EMPFlowJets.root macros/ExampleConfig_nathan.config MuTerm average
# these lines can be used to calibrate using IJT.


# To see closure plots, edit the config with the new NPV and Mu corrections (and change description/version!) obtained in the previous step (residual/ MuTerm and NPVTerm):
#	Option1_13122020_139fb_ptfrom10_v2AntiKt4EMPFlowJets.Description: Option1_13122020_139fb_ptfrom10_v2 mu- and N_{PV}-dependent jet pileup corrections
#	Option1_13122020_139fb_ptfrom10_v2AntiKt4EMPFlowJets.AbsEtaBins: 0.000 0.900 1.200 1.500 1.800 2.400 2.800 3.200 3.500 4.000 4.300 6.000
#	Option1_13122020_139fb_ptfrom10_v2AntiKt4EMPFlowJets.NPVTerm.AntiKt4EMPFlowJets: 0.005 0.027 0.169 -0.013 -0.076 0.083 -0.174 -0.004 -0.201 -0.328 0.060 90320.894
#	Option1_13122020_139fb_ptfrom10_v2AntiKt4EMPFlowJets.MuTerm.AntiKt4EMPFlowJets: -0.025 -0.024 -0.079 0.032 -0.047 -0.018 0.130 0.024 0.080 0.107 0.003 4402.134
# and run again:
python DeriveResidualCorrection.py



# 3D #
# edit conifg and input file and run
# 3D (1D False in DeriveResidualCorrection.py)
python DeriveResidualCorrection.py

# Now do the Smoothing
cd scripts/
python Histos3Dto2D.py -i ../TH3histograms/TH3_Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.root -o ../TH3histograms/TH3_Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets_Smoothing.root
# this prints lots of errors but output looks fine...

# Finally, from the output of the smoothing you can derive the correction. The first time you need to compile the code with:
cd ../CorrelatedCorrection
g++ -o DeriveResidualCorr CorrelatedResidual.cxx `root-config --cflags --glibs`
# fit:
./DeriveResidualCorr ../TH3histograms/TH3_Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets_Smoothing.root ../macros/ExampleConfig_nathan.config th2 1
# this looks fine too... it created:
# 3DcorrParams_Residual3D_R22_testAntiKt4EMPFlowJets.root and residualCalibParameters.root (this one will be the config for the calibration!)
# inside the CorrelatedCorrection folder.

# make closure (not running)
./MkClosurePlots ../R22_results_reweighted/TH3_Option1_R22_test2AntiKt4EMPFlowJets.root ../macros/ExampleConfig_nathan.config "NPVTerm" "inclusive"



# To see closure plots, edit the config with the new NPV and Mu corrections obtained in the previous step (residual/ MuTerm and NPVTerm):
#	Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.Description: Option1_13122020_139fb_ptfrom10_v2_3D mu- and N_{PV}-dependent jet pileup corrections
#	Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.AbsEtaBins: 0.000 0.900 1.200 1.500 1.800 2.400 2.800 3.200 3.500 4.000 4.300 6.000
#	Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.NPVTerm.AntiKt4EMPFlowJets: 0.029 0.024 0.214 -0.025 0.046 0.134 0.209 -0.105 -0.193 -0.069 0.035 59286.796
#	Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.MuTerm.AntiKt4EMPFlowJets: -0.026 -0.032 -0.097 0.009 -0.111 -0.079 -0.139 0.047 0.052 -0.046 0.008 13036.794

# Edit the Histos3DSubs3DCorrection.cxx file in order to inlcude correctly the header:
# #include "bcidtools.h"              ->    should be    ->    #include "../bcidtools.h"

# Also the file Histos3DSubs3DCorrection.cxx has to be edited:
# TString ptvariable("TruePtBin");    ->    should be    ->    std::string ptvariable("TruePtBin");
# TString etavariable("TrueEtaBin");  ->    should be    ->    std::string etavariable("TrueEtaBin");

# Also
# Double_t         weight=0;          ->    should be    ->     Float_t         weight=0;

# Run from inside CorrelatedCorrection:
root -l 'Histos3DSubs3DCorrection.cxx+( "../ntuples/MC16_r22Sample_Rel22_noCalib_171221.txt", "../macros/ExampleConfig_nathan.config", "ApplyArea", "ApplyWeight", "../CorrelatedCorrection/3DcorrParams_Option1_13122020_139fb_ptfrom10_v2_3DAntiKt4EMPFlowJets.root")'

