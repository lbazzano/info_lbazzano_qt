How to get and update trigger counts:

Since you already have 1 commit, I am assuming you did the whole setup as described in 
https://atlassoftwaredocs.web.cern.ch/gittutorial/git-clone/
https://atlassoftwaredocs.web.cern.ch/gittutorial/branch-and-change/
and already (sparse) checked out the JetCalibTools package

Checkout the trigger validation packages, but first make sure you are working off the latest master (or 22.0) version:

cd athena/
git fetch upstream
git rebase upstream/22.0
lsetup git
git atlas addpkg TrigAnalysisTest
git atlas addpkg TrigP1Test
cd ..
vim package_filters.txt
Add new packages so that now have: 
“
+ Reconstruction/Jet/JetCalibTools
+ Trigger/TrigValidation/TrigP1Test
+ Trigger/TrigValidation/TrigAnalysisTest
“
cd build
asetup Athena,22.0,latest
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
make -j4
source x86_64-centos7-gcc11-opt/setup.sh 
cd ../run
(Now run 2 trigger tests:)
test_trigAna_RDOtoRDOTrig_v1Dev_build.py
test_trigP1_v1Dev_decodeBS_build.py


If everything ran alright but trigger counts changed then the result will inform you of this and give you instructions on how to apply the changes to the trigger count (git apply <path_to_patch_file>)


####################### what I do
setupATLAS
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git
cd athena
git rebase upstream/22.0 
asetup Athena,22.0,latest
git atlas addpkg TrigAnalysisTest
git atlas addpkg TrigP1Test
git atlas addpkg TriggerMenuMT
git atlas addpkg JetCalibTools
#git checkout lbazzano-triggercounting 
#git fetch upstream
cd ..
mkdir build run
vim package_filters.txt # EDIT
cd build
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
make -j4
asetup Athena,22.0,latest
source x86_*/setup.sh
cd ../run

#############################
setupATLAS
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git
cd athena
git fetch upstream
git rebase upstream/22.0
git atlas addpkg TrigAnalysisTest
git atlas addpkg TrigP1Test
git atlas addpkg TriggerMenuMT
git atlas addpkg JetCalibTools
cd ../build
asetup Athena,22.0,latest
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
make -j4
source x86_*/setup.sh
cd ../run
(Now run 2 trigger tests:)
test_trigAna_RDOtoRDOTrig_v1Dev_build.py
test_trigP1_v1Dev_decodeBS_build.py

##########################################
|-- athena
| |-- Projects
| |-- Reconstruction
| `-- Trigger
|-- build
| |-- CMakeFiles
| |-- Reconstruction
| |-- Testing
| |-- Trigger
| `-- x86_64-centos7-gcc11-opt
`-- run
|-- test_trigAna_RDOtoRDOTrig_v1Dev_build
|-- test_trigP1_v1Dev_decodeBS_build
`-- test_trig_mc_v1Dev_slice_jet_build 
asetup Athena,22.0,latest 
in top level directory 
https://atlassoftwaredocs.web.cern.ch/gittutorial/git-clone/ 
